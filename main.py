
# Snake water gun game:
import random
lst = ['Snake','Water','Gun']
win = 0
loss = 0
draw= 0


# def getResult(comp,user):
#     win = 0
#     loss = 0
#     draw = 0
#     if comp == 'Snake':
#         if user == 's':
#             print(f'Computer choose {comp}, Its a \'DRAW\'')
#             draw += 1
#         elif user == 'w':
#             print(f'Computer choose {comp}, Its your \'LOSS\'')
#             loss += 1
#         else:
#             print(f'Computer choose {comp}, Its your \'Win\'')
#             win += 1
#     elif comp == 'Water':
#         if user == 's':
#             print(f'Computer choose {comp}, Its your \'Win\'')
#             win += 1
#         elif user == 'w':
#             print(f'Computer choose {comp}, Its a \'DRAW\'')
#             draw += 1
#         else:
#             print(f'Computer choose {comp}, Its your \'LOSS\'')
#             loss += 1
#     else:
#         if user == 's':
#             print(f'Computer choose {comp}, Its your \'LOSS\'')
#             loss += 1
#         elif user == 'w':
#             print(f'Computer choose {comp}, Its your \'Win\'')
#             win += 1
#         else:
#             print(f'Computer choose {comp}, Its a \'DRAW\'')
#             draw += 1
#     return win,loss,draw

# getResult() short form
def getResult(comp,user) :
    if (comp == 'Snake' and user == 's') or (comp == 'Water' and user == 'w') or (comp == 'Gun' and user == 'g') :
        print(f'Computer choose {comp}, Its a \'DRAW\'')
        global draw
        draw +=1
    elif (comp == 'Snake' and user == 'w') or (comp == 'Water' and user == 'g') or (comp == 'Gun' and user == 's') :
        print(f'Computer choose {comp}, Its your \'Win\'')
        global win
        win +=1
    else:
        print(f'Computer choose {comp}, Its your \'LOSS\'')
        global loss
        loss +=1

t = int(input('How many times you want to play : '))
for i in range(t):
    comp = random.choice(lst)
    user = input('Type(s for \'Snake\', w for \'Water\' and g for \'Gun\') : ')

    # for first getResult() Function
    # w,l,d = getResult(comp,user)
    # win +=w
    # loss += l
    # draw += d
    getResult(comp,user)

print(f'Game result is : {win} Win,{loss} Loss and {draw} Draw')
